import { Router } from 'express';
import * as marksController from './marks.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { markCreateSchema } from './mark.schema';

const router = Router();

router.post(
  '/',
  validator.body(markCreateSchema),
  controllerWrapper(marksController.addMarkForStudent)
);

router.get('/student', controllerWrapper(marksController.getMarksOfStudent));

router.get('/course', controllerWrapper(marksController.getMarksOfCourse));

export default router;

