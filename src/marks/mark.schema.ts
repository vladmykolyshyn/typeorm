import Joi from 'joi';
import { IMarkResponse } from './types/mark.response.interface'; 

export const markCreateSchema = Joi.object<Omit<IMarkResponse, 'id'>>({
  mark: Joi.number().integer().min(1).max(100).required(),
  course_id: Joi.number().required(),
  student_id: Joi.number().required(),
  lector_id: Joi.number().required(),
});
