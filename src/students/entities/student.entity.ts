import { Entity, Column, ManyToOne, JoinColumn, OneToMany, Index } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Group } from '../../groups/entities/group.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'students' })
export class Student extends CoreEntity {
  @Index("idx_student_name")
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  surname: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @Column({
    type: 'numeric',
    nullable: false,
  })
  age: number;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  imagePath: string;

  @Column({
    type: 'integer',
    nullable: true,
    name: 'group_id',
  })
  groupId: number;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @OneToMany(() => Mark, (mark) => mark.student)
  marks: Mark[];
}
