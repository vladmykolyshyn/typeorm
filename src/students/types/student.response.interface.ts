import { IStudent } from './student.interface';

export interface IStudentResponse extends IStudent {
  groupId: number;
}
