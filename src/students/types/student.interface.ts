export interface IStudent {
  id: number;
  email: string;
  name: string;
  surname: string;
  age: number;
  imagePath: string;
}
