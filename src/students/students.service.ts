import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { IStudent } from './types/student.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entities/student.entity';
import { DeleteResult, UpdateResult } from 'typeorm';
import { IStudentResponse } from './types/student.response.interface';
import { groupsRepository } from '../groups/groups.service';

export const studentsRepository = AppDataSource.getRepository(Student);

export const getAllStudents = async (): Promise<Student[]> => {
  const students = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.createdAt as "createdAt"',
      'student.updatedAt as "updatedAt"',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.imagePath as imagePath',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .getRawMany();
  return students;
};

export const getStudentsByName = async (name: string): Promise<Student[]> => {
  const students = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.createdAt as "createdAt"',
      'student.updatedAt as "updatedAt"',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.imagePath as imagePath',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where('student.name = :name', { name })
    .getRawMany();
  
  if (students.length === 0) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'No students found with the specified name'
    );
  }

  return students;
};

export const getStudentById = async (id: number): Promise<Student> => {
  const student = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.createdAt as "createdAt"',
      'student.updatedAt as "updatedAt"',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.imagePath as imagePath',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where('student.id = :id', { id })
    .getRawOne();

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return student;
};

export const createStudent = async (
  studentCreateSchema: Omit<IStudent, 'id'>
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: {
      email: studentCreateSchema.email,
    },
  });
  if (student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this email already exists'
    );
  }
  return studentsRepository.save(studentCreateSchema);
};

export const updateStudentById = async (
  id: number,
  studentUpdateSchema: Partial<IStudentResponse>
): Promise<UpdateResult> => {
  const { groupId } = studentUpdateSchema;

  const group = await groupsRepository.findOne({ where: { id: groupId } });
  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  const result = await studentsRepository.update(id, studentUpdateSchema);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }


  return result;
};

export const addGroupToStudent = async (
  id: number,
  studentGroupSchema: Pick<IStudentResponse, 'groupId'>
): Promise<UpdateResult> => {
  const { groupId } = studentGroupSchema;

  const group = await groupsRepository.findOne({ where: { id: groupId } });
  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  const addedGroup = await studentsRepository.update(id, studentGroupSchema);
  
  if (!addedGroup.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return addedGroup;
};

export const deleteStudentById = async (id: number): Promise<DeleteResult> => {
  const result = await studentsRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return result;
};

