import { MigrationInterface, QueryRunner } from "typeorm";

export class ChangeNullableOfIdForJoinInMarkEntity1688316606257 implements MigrationInterface {
    name = 'ChangeNullableOfIdForJoinInMarkEntity1688316606257'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fa21194644d188132582b0d1a3f"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_67ca379415454fe438719515529"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_5226e1592e6291dbe7a07640346"`);
        await queryRunner.query(`ALTER TABLE "marks" ALTER COLUMN "lector_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "marks" ALTER COLUMN "course_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "marks" ALTER COLUMN "student_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "marks" ADD CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "marks" ADD CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "marks" ADD CONSTRAINT "FK_5226e1592e6291dbe7a07640346" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fa21194644d188132582b0d1a3f" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_67ca379415454fe438719515529" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_67ca379415454fe438719515529"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fa21194644d188132582b0d1a3f"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_5226e1592e6291dbe7a07640346"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8"`);
        await queryRunner.query(`ALTER TABLE "marks" ALTER COLUMN "student_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "marks" ALTER COLUMN "course_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "marks" ALTER COLUMN "lector_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "marks" ADD CONSTRAINT "FK_5226e1592e6291dbe7a07640346" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "marks" ADD CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "marks" ADD CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_67ca379415454fe438719515529" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fa21194644d188132582b0d1a3f" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
