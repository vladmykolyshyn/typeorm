import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { Course } from '../courses/entities/course.entity';
import { Lector } from './entities/lector.entity';
import { ILector } from './types/lector.interface';

export const lectorsRepository = AppDataSource.getRepository(Lector);

export const getAllLectors = async (): Promise<Lector[]> => {
  const lectors = await lectorsRepository
    .createQueryBuilder('lector')
    .getMany();
  return lectors;
};

export const getLectorById = async (id: number): Promise<Lector> => {
  const lector = await lectorsRepository
    .createQueryBuilder('lector')
    .leftJoinAndSelect('lector.courses', 'course')
    .where('lector.id = :id', { id })
    .getOne();

  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  return lector;
};

export const getCoursesTaughtByLector = async (id: number): Promise<Course[]> => {
  const lector = await lectorsRepository
    .createQueryBuilder('lector')
    .leftJoinAndSelect('lector.courses', 'course')
    .where('lector.id = :id', { id })
    .getOne();

  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  return lector.courses;
};

export const createLector = async (
  lectorCreateSchema: Omit<ILector, 'id'>
): Promise<Lector> => {
  const lector = await lectorsRepository.findOne({
    where: {
      email: lectorCreateSchema.email,
    },
  });
  if (lector) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Lector with this email already exists'
    );
  }
  return lectorsRepository.save(lectorCreateSchema);
};