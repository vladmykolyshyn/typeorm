import { ICourse } from './course.interface';

export interface ICourseResponse extends ICourse {
    lectorId: number;
}
