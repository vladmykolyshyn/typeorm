import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ICourseResponse } from './course.response.interface';

export interface ICourseAddLectorRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Pick<ICourseResponse, 'id' | 'lectorId'>;
}