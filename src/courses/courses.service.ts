import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { lectorsRepository } from '../lectors/lectors.service';
import { Course } from './entities/course.entity';
import { ICourse } from './types/course.interface';
import { ICourseResponse } from './types/course.response.interface';

export const coursesRepository = AppDataSource.getRepository(Course);

export const getAllCourses = async (): Promise<Course[]> => {
  const courses = await coursesRepository
    .createQueryBuilder('course')
    .getMany();
  return courses;
};

export const createCourse = async (
  courseCreateSchema: Omit<ICourse, 'id'>
): Promise<Course> => {
  const course = await coursesRepository.findOne({
    where: {
      name: courseCreateSchema.name,
    },
  });
  if (course) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Course with this name already exists'
    );
  }
  return coursesRepository.save(courseCreateSchema);
};

export const addLectorToCourse = async (
  courseId: number,
  courseLectorSchema: Pick<ICourseResponse, 'lectorId'>
): Promise<Course> => {
  const course = await coursesRepository.findOne({ where: { id: courseId } });
  if (!course) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  const lector = await lectorsRepository.findOne({
    where: { id: courseLectorSchema.lectorId },
  });
  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  course.lectors = course.lectors || [];
  course.lectors.push(lector);
  return coursesRepository.save(course);
};
